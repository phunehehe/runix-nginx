#!/usr/bin/env bash
set -efuxo pipefail

NEW_PATH=''
for p in coreutils git gnused nix rsync
do
  NEW_PATH=$(nix-build --no-out-link '<nixpkgs>' --attr $p)/bin:$NEW_PATH
done
PATH=$NEW_PATH

update_submodules() {
    directory="$1"
    git -C "$directory" submodule sync
    git -C "$directory" submodule update --init --recursive
}

fetch() {
    repository="$1"
    directory="$2"
    branch="${3:-master}"
    if [[ -e "$directory" ]]
    then
        git -C "$directory" fetch
        git -C "$directory" checkout "origin/$branch"
        update_submodules "$directory"
    else
        git clone --recursive --branch "$branch" "$repository" "$directory"
    fi
}

# gitlab.com shared runners seem to put the source in a read-only location
src_dir=$(readlink -f "$(dirname "$0")/..")
build_dir=/tmp/build
rsync --delete --links --recursive "$src_dir"/ $build_dir

cache_dir=$build_dir/test/cache

fetch https://gitlab.com/phunehehe/runix.git $cache_dir/runix

update_submodules "$build_dir"
nix-build --no-out-link "$build_dir/test/runix/default.nix"
