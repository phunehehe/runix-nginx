# [Nginx](http://nginx.org/) for [Runix](https://gitlab.com/phunehehe/runix)

[![build status](https://gitlab.com/phunehehe/runix-nginx/badges/master/build.svg)](https://gitlab.com/phunehehe/runix-nginx/commits/master)

## Incoming exports

  - `*.nginx.http-include`: this app collects the export `nginx.http-include`
    from all apps and adds an `include` line for each of them in the `http`
    section in Nginx's config file.

## Outgoing exports

  - `hstsConfig`:
    [HSTS](https://en.wikipedia.org/wiki/HTTP_Strict_Transport_Security) config
    header, suitable for embedding into another app's Nginx config
  - `reloadCommand`: a command that can be used in other apps to tell Nginx to
    reload e.g. if the app's Nginx config has been rebuilt separately from
    Nginx itself
  - `staticFilesConfig`: a location block that allows caching of static files,
    suitable for embedding into another app's Nginx config