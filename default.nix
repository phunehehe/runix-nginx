{ exports, home, lib, pkgs, ... }:

with builtins;

let

  inherit (lib) concatMapStrings mapAttrsToList;

  dhparam = "${home}/dhparam.pem";

  configuration-file = pkgs.writeText "nginx.conf" ''

    daemon           off;
    error_log        stderr;
    pid              ${home}/nginx.pid;
    worker_processes auto;

    events {
      # Nginx (1.10.1) really wants this empty section
    }

    http {

      log_format bunyan '{'
        '"name": "nginx/$nginx_version",'
        '"hostname": "$hostname",'
        '"pid": "$pid",'
        '"level": 30,'
        '"time": "$time_iso8601",'
        '"v": 0,'
        '"msg": "access",'
        '"remoteAddress": "$remote_addr",'
        '"remotePort": "$remote_port",'
        '"req": {'
          '"method": "$request_method",'
            '"url": "$request_uri",'
            '"headers": {'
              '"user-agent": "$http_user_agent",'
                '"host": "$host",'
                '"referrer": "$http_referrer",'
                '"username": "$remote_user",'
                '"content-type": "$content_type",'
                '"content-length": "$content_length",'
                '"accept": "$http_accept",'
                '"accept-language": "$http_accept_language",'
                '"accept-encoding": "$http_accept_encoding"'
            '},'
            '"serverProtocol": "$server_protocol",'
            '"requestLength": "$request_length"'
        '},'
        '"res": {'
          '"statusCode": "$status",'
            '"headers": {'
              '"server": "nginx/$nginx_version",'
                '"content-length": "$body_bytes_sent"'
            '},'
            '"responseTime": "$request_time"'
        '}'
      '}';

      access_log /dev/stdout bunyan;
      include    ${pkgs.nginx}/conf/mime.types;

      sendfile   on;
      tcp_nopush on;

      client_body_temp_path ${home}/temp;
      fastcgi_temp_path     ${home}/temp;
      proxy_temp_path       ${home}/temp;
      scgi_temp_path        ${home}/temp;
      uwsgi_temp_path       ${home}/temp;

      gzip              on;
      gzip_proxied      any;
      gzip_types
                        application/javascript
                        application/json
                        application/x-javascript
                        application/xml
                        application/xml+rss
                        text/css
                        text/javascript
                        text/plain
                        text/xml
      ;

      ssl_dhparam               ${dhparam};
      ssl_prefer_server_ciphers on;
      ssl_session_cache         shared:SSL:10m;
      ssl_stapling              on;
      ssl_stapling_verify       on;
      ssl_trusted_certificate   ${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt;

      ${http-includes}
    }
  '';

  nginx = "${pkgs.nginx}/bin/nginx -c ${configuration-file}";

  http-includes = concatMapStrings (f: ''
    include ${f};
  '') (pkgs.collectAttrByPath ["nginx" "http-include"] exports);

in {

  run = pkgs.writeBash "nginx" ''
    [[ -e ${dhparam} ]] || ${pkgs.openssl}/bin/openssl dhparam -out ${dhparam} 4096
    exec ${nginx}
  '';

  exports = rec {

    reloadCommand = "${nginx} -s reload";

    hstsConfig = ''
      add_header Strict-Transport-Security "max-age=15552000; includeSubDomains; preload";
    '';

    staticFilesConfig = ''
      location ~ \.(css|gif|jpeg|jpg|js|png)$ {
        expires 1w;
      }
    '';

    listenDual = port: listenDualExtras port [];
    listenDualExtras = port: extras:
    let
      things = concatStringsSep " " ([(toString port)] ++ extras);
    in ''
      listen      ${things};
      listen [::]:${things};
    '';
  };
}
